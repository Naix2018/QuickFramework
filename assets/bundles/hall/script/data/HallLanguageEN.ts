export let HALL_EN = {

    language: cc.sys.LANGUAGE_ENGLISH,
    data: {
        hall_view_game_name: [
            'game1',
            'game2',
            `BATTLE
CITY`,
            'Load Test',
            'Net Test',
            "Aim Line",
            "Node Pool",
            "Shader",
            "三消"
        ],
        hall_view_broadcast_content: '[broadcast] congratulations!',
        hall_view_nogame_notice: '【{0}】developing!!!',
        test: " test : {0}-->{1}-->{2}-->{3}-->",
    }
}