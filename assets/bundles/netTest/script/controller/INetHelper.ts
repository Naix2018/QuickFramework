export interface INetHelper{
    sendProtoMessage( hello : string);
    sendJsonMessage( hello : string);
    sendBinaryMessage( hello : string);
}
